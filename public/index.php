<?php
declare(strict_types=1);
/**
 * @author Valentin Plehanov (Takamura) valentin@plehanov.pro
 */

include_once '..' . DIRECTORY_SEPARATOR . 'bootstrap.php';

// Routes ---------------------------------------------------------------------
echo (new Route)->run($_SERVER?:[]);
