/**
 * Created by takamura on 08.07.17.
 */

$(document).ready(function () {

    $('#task_employee').on('change', function () {
        $('[data-tm-employee]').html($(this).val());
    });
    $('#task_email').on('change', function () {
        $('[data-tm-email]').html($(this).val());
    });
    $('#task_content').on('change', function () {
        $('[data-tm-content]').html($(this).val());
    });

    $('#task_file').on('change', function (e) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('[data-tm-image]').attr('src', e.target.result);
        };
        reader.readAsDataURL(this.files[0]);
    });
});

