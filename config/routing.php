<?php
declare(strict_types=1);

Route::root('/', 'BasicController@index');
Route::post('/', 'BasicController@create');

Route::post('/login', 'AuthController@login');
Route::get('/logout', 'AuthController@logout');

if (Helpers\AuthHelper::isAdmin()) {
    Route::get('/edit', 'BasicController@edit');
    Route::post('/edit', 'BasicController@update');
    Route::get('/delete', 'BasicController@delete');
}
