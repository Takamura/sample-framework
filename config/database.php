<?php
declare(strict_types=1);

return [
    'host' => '192.168.1.12',
    'database' => 'sample_db',
    'username' => 'user',
    'password' => 'zxcvbnm',
    'charset' => 'utf8',
];