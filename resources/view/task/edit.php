<div class="jumbotron">
    <h1>Задача #<?=$task->id?></h1>

    <form method="post" action="/edit?id=<?=$task->id?>" enctype="multipart/form-data">
        <div class="form-group">
            <label for="task_employee">Исполнитель</label>
            <input type="text" class="form-control" id="task_employee" name="task[username]" placeholder="John Doe" required value="<?=$task->username?>">
        </div>
        <div class="form-group">
            <label for="task_email">Почта</label>
            <input type="email" class="form-control" id="task_email" name="task[email]" placeholder="john@sample.com" required value="<?=$task->email?>">
        </div>
        <div class="form-group">
            <label for="task_content">Суть задания</label>
            <textarea class="form-control" rows="3" id="task_content" name="task[content]" placeholder="You need to complete..." required><?=$task->content?></textarea>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="task[is_complete]" <?=$task->is_complete ? 'checked' : ''?>> Задача готова? :)
            </label>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="task_file">Картинка</label>
                    <input type="file" id="task_file" name="task[file]" accept="image/*">
                </div>
            </div>
            <div class="col-sm-6">
                <?php if ($task->filename):?>
                    <img class="task-image" src="/uploads/<?=$task->filename?>"/>
                <?php endif;?>
            </div>
        </div>
        <div class="btn-group" role="group" aria-label="...">
            <button type="submit" class="btn btn-primary">Изменить</button>
            <a class="btn btn-danger" href="/delete?id=<?=$task->id?>" onclick="return confirm('Вы подтверждаете удаление?')">Удалить</a>
        </div>
    </form>
</div>
