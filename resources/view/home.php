<div>
    <h1>Список задач</h1>
    <table class="table table-striped" id="table">
        <thead>
        <tr>
            <th>#</th>
            <th><?= \Helpers\ViewHelper::arrows($sortName, $sortType, 'username', "?page={$page}", 'Исполнитель ') ?></th>
            <th><?= \Helpers\ViewHelper::arrows($sortName, $sortType, 'email', "?page={$page}", 'Почта ') ?></th>
            <th class="col-sm-1"><?= \Helpers\ViewHelper::arrows($sortName, $sortType, 'is_complete', "?page={$page}", 'Статус ') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ((array)$tasks as $task): ?>
            <tr>
                <th scope="row"><?= $task->id?></th>
                <td><?= $task->username?></td>
                <td>
                    <?= $task->email?>
                    <?php if (\Helpers\AuthHelper::isAdmin()): ?>
                        &mdash; <a href="/edit?id=<?= $task->id?>">Редактировать</a>
                    <?php endif; ?>
                </td>
                <td><span class="glyphicon glyphicon-<?= (bool)$task->is_complete ? 'ok' : 'minus'?>"></span> </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <?php if ($task->filename):?>
                        <img class="task-image" src="/uploads/<?=$task->filename?>"/>
                    <?php endif;?>
                </td>
                <td colspan="2">
                    <?= $task->content?>
                </td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
    <br/>
    <br/>
    <nav aria-label="...">
        <ul class="pager">
            <li class="<?= $page <= 0 ? 'disabled': ''?>">
                <?= \Helpers\ViewHelper::page("/?sort={$sortName}-{$sortType}", $page - 1, '<span aria-hidden="true">&larr;</span> Предыдущая') ?>
            </li>
            <li class="<?= $page >= $maxPage ? 'disabled': ''?>">
                <?= \Helpers\ViewHelper::page("/?sort={$sortName}-{$sortType}", $page + 1, 'Следущая <span aria-hidden="true">&rarr;</span>') ?>
            </li>
        </ul>
    </nav>
</div>

<div class="well">
    <h1>Новая задача</h1>

    <form method="post" action="/" enctype="multipart/form-data" data-form>
        <div class="form-group">
            <label for="task_employee">Исполнитель</label>
            <input type="text" class="form-control" id="task_employee" name="task[username]" placeholder="Максим Исаев" required>
        </div>
        <div class="form-group">
            <label for="task_email">Почта</label>
            <input type="email" class="form-control" id="task_email" name="task[email]" placeholder="maksim@isaev.ru" required>
        </div>
        <div class="form-group">
            <label for="task_content">Суть задания</label>
            <textarea class="form-control" rows="3" id="task_content" name="task[content]" placeholder="Вам необходимо сделать..." required></textarea>
        </div>
        <div class="form-group">
            <label for="task_file">Картинка</label>
            <input type="file" id="task_file" name="task[file]" accept="image/*">
        </div>

        <div class="btn-group" role="group" aria-label="...">
            <button type="button" class="btn btn-info" data-toggle="modal" data-target=".bs-example-modal-lg">
                Предварительный просмотр
            </button>

            <button type="submit" class="btn btn-primary">Добавить</button>
        </div>
    </form>
</div>


<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-preview-block>
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Предварительный просмотр</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped" id="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Исполнитель</th>
                        <th>Почта</th>
                        <th class="col-sm-1">Статус</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row"> &bull; </th>
                        <td data-tm-employee></td>
                        <td data-tm-email></td>
                        <td><span class="glyphicon glyphicon-minus"></span> </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td><img class="task-image" src="" data-tm-image></td>
                        <td colspan="2" data-tm-content></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
