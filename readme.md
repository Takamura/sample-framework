**Запуск**

```
php -S 0.0.0.0:8081 -t public/
```

**Миграция**

```
php migrate.php up
```

**Настройка базы**

```
docker run --name sample_db -e MYSQL_ROOT_PASSWORD=root -p 3306:3306 -d mysql
docker exec -it sample_db bash

mysql -u root -p
SHOW DATABASES;
CREATE DATABASE sample_db;

CREATE USER 'user'@'*' IDENTIFIED BY 'zxcvbnm';
GRANT ALL PRIVILEGES ON * . * TO 'user'@'*';
FLUSH PRIVILEGES;
```

Права в файле `config/database.php`

**Админ панель**

Права на редактирование в файле `config/auth.php`