<?php
declare(strict_types=1);
/**
 * @author Valentin Plehanov (Takamura) valentin@plehanov.pro
 */


// Define ---------------------------------------------------------------------
define('DS', DIRECTORY_SEPARATOR);

$root = realpath(__DIR__) . DS;
chdir($root);
define('APP_ROOT', $root);
define('APP_KERNEL', APP_ROOT . 'kernel' . DS);
define('APP_CONFIG', APP_ROOT . 'config' . DS);
define('APP_TEMPLATE', APP_ROOT . 'resources' . DS . 'view' . DS);
define('APP_UPLOADS', APP_ROOT . 'public' . DS . 'uploads' . DS);


// Autoloader ---------------------------------------------------------------------
include APP_KERNEL . 'Autoloader.php';
Autoloader::initialize();


// Helpers ---------------------------------------------------------------------
/**
 * @param string $template
 * @param array  $parameters
 *
 * @return string
 */
function view(string $template, array $parameters = []): string
{
    return Response::build('layout', str_replace('.', '/', $template), $parameters);
}

/**
 * @param array ...$parameters
 */
function dd(...$parameters)
{
    echo '<pre>';
    foreach ($parameters as $parameter) {
        var_dump($parameter);
        echo "\n\n";
    }
    debug_print_backtrace();
    echo '</pre>';
    die();
}