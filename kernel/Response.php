<?php
declare(strict_types=1);

/**
 * Class Response
 * @author Valentin Plehanov (Takamura) valentin@plehanov.pro
 */
class Response
{
    /**
     * @param string $layoutName
     * @param string $templateName
     * @param array  $parameters
     *
     * @return string
     * @throws \RuntimeException
     */
    public static function build(string $layoutName, string $templateName, array $parameters = []): string
    {
       $content = self::page($templateName, $parameters);

       return self::layout($layoutName, $content);
    }

    /**
     * @param string $templateName
     * @param array  $parameters
     *
     * @return string
     * @throws \RuntimeException
     */
    public static function page(string $templateName, array $parameters = []): string
    {
        ob_start();

        extract($parameters);
        $templateFile = static::getTemplate($templateName);
        require $templateFile;

        $out = ob_get_contents();
        ob_end_clean();

        return $out;
    }

    /**
     * @param string $layoutName
     * @param string $content - загрузка данных в слой
     *
     * @return string
     * @throws \RuntimeException
     */
    protected static function layout(string $layoutName, string $content): string
    {
        ob_start();

        $layoutFile = static::getTemplate($layoutName);
        require $layoutFile;

        $out = ob_get_contents();
        ob_end_clean();

        return $out;
    }

    /**
     * @param string $templateName
     *
     * @return string
     * @throws \RuntimeException
     */
    protected static function getTemplate(string $templateName): string
    {
        $templateFile = APP_TEMPLATE . str_replace('.', DS, $templateName). '.php';
        if (is_readable($templateFile)) {
            return $templateFile;
        }
        throw new \RuntimeException("Not found template {$templateFile}");
    }
}