<?php
declare(strict_types=1);

/**
 * Class Autoloader
 * @author Valentin Plehanov (Takamura) valentin@plehanov.pro
 */
class Autoloader
{
    /**
     * @var array
     */
    protected $directories = ['app', 'kernel'];

    /**
     * @return bool
     */
    public static function initialize(): bool
    {
        return spl_autoload_register([new Autoloader, 'load']);
    }

    /**
     * @param string $className
     */
    protected function load(string $className)
    {
        $classPath = str_replace('\\', DS, "{$className}.php");
        foreach ($this->directories as $directory) {
            $path = APP_ROOT . $directory . DS . $classPath;
            if (is_readable($path)) /** @noinspection PhpIncludeInspection */ {
                require_once($path);
            }
        }
    }
}
