<?php
declare(strict_types=1);

/**
 * Class Route
 * @author Valentin Plehanov (Takamura) valentin@plehanov.pro
 */
class Route
{
    /**
     * @var string
     */
    protected static $folderPrefix = 'Controllers\\';

    /**
     * Route constructor.
     */
    public function __construct()
    {
        $this->loadConfig();
    }

    /**
     * @var array
     */
    protected static $routes = [];


    /**
     * @param string $uri
     * @param int    $code
     */
    public static function redirect($uri = '/', int $code = 303): void
    {
        header("Location: {$uri}",TRUE,$code);
        exit;
    }

    /**
     * @param string $URI
     * @param string $resolver
     */
    public static function root(string $URI, string $resolver): void
    {
        static::$routes['GET'][$URI] = static::$folderPrefix . $resolver;
        static::$routes['ROOT'] = static::$folderPrefix . $resolver;
    }

    /**
     * @param string $URI
     * @param string $resolver
     */
    public static function get(string $URI, string $resolver): void
    {
        static::$routes['GET'][$URI] = static::$folderPrefix . $resolver;
    }

    /**
     * @param string $URI
     * @param string $resolver
     */
    public static function post(string $URI, string $resolver): void
    {
        static::$routes['POST'][$URI] = static::$folderPrefix . $resolver;
    }

    /**
     *
     */
    protected function loadConfig(): void
    {
        require APP_CONFIG . 'routing.php';
    }

    /**
     * @param array $serverArgs
     *
     * @return array
     */
    protected function extractParams(array $serverArgs): array
    {
        // Есть всегда
        //["REQUEST_URI"]=> string(15) "/sample?foo=bar"
        //["REQUEST_METHOD"]=> string(3) "GET"
        // Перманентно
        //["QUERY_STRING"]=> string(7) "foo=bar"
        //["PATH_INFO"]=> string(7) "/sample"
        return [
            'REQUEST_METHOD' => $serverArgs['REQUEST_METHOD'],
            'PATH_INFO' => array_key_exists('PATH_INFO', $serverArgs) ? $serverArgs['PATH_INFO'] : $serverArgs['REQUEST_URI'],
            'QUERY_STRING' => array_key_exists('QUERY_STRING', $serverArgs) ? $serverArgs['QUERY_STRING'] : '',
        ];
    }

    /**
     * @param string $currentURI
     * @param string $method
     *
     * @return array
     * @throws \RuntimeException
     */
    protected function findResolver(string $currentURI, string $method): array
    {
        $currentURI = mb_strtolower($currentURI);
        if ($currentURI !== '/') {
            $currentURI = rtrim($currentURI, '/');
        }
        $currentResolver = null;
        foreach ((array)static::$routes[$method] as $URI => $resolver) {
            if ($URI === $currentURI) {
                $currentResolver = $resolver;
                break;
            }
        }
        if (!$currentResolver) {
            if (static::$routes['ROOT']) {
                $currentResolver = static::$routes['ROOT'];
            } else {
                throw new RuntimeException('Not found route by current url');
            }
        }

        return explode('@', $currentResolver);
    }

    /**
     * @param array $serverArgs
     *
     * @return string
     * @throws \RuntimeException
     */
    public function run(array $serverArgs): string
    {
        $args = $this->extractParams($serverArgs);
        parse_str($args['QUERY_STRING'], $parameters);

        $currentURI = str_replace("?{$args['QUERY_STRING']}", '', $args['PATH_INFO']);
        [$controller, $action] = $this->findResolver($currentURI, $args['REQUEST_METHOD']);

        return (new $controller)->$action($parameters);
    }
}