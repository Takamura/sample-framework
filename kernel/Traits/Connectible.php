<?php
declare(strict_types=1);

namespace Traits;


/**
 * Trait Connectible
 * @author Valentin Plehanov (Takamura) valentin@plehanov.pro
 * @package Traits
 */
trait Connectible
{
    /**
     * @var \PDO
     */
    protected static $dbh;

    /**
     * @var array
     */
    protected static $dbParameters = [];

    /**
     * Connectible constructor.
     */
    public function __construct()
    {
        self::connection();
    }

    /**
     *
     */
    protected static function loadConfig(): void
    {
        static::$dbParameters = require APP_CONFIG . 'database.php';
    }

    /**
     *
     */
    protected static function connection(): void
    {
        if (static::$dbh) {
            return;
        }

        static::loadConfig();

        $dsn = 'mysql:host=' . static::$dbParameters['host']
            . ';dbname=' . static::$dbParameters['database']
            . ';charset=' . static::$dbParameters['charset'];
        $opt = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES => false,
        ];

        try {
            static::$dbh = new \PDO($dsn, static::$dbParameters['username'], static::$dbParameters['password'], $opt);
        } catch (\PDOException $e) {
            die('Connection is broken: ' . $e->getMessage());
        }
    }
}
