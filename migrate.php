<?php
declare(strict_types=1);

use Traits\Connectible;

require 'bootstrap.php';

/**
 * Class Migrate
 * @author Valentin Plehanov (Takamura) valentin@plehanov.pro
 */
class Migrate
{
    use Connectible;

    public function up()
    {
        $sql = '
              CREATE TABLE IF NOT EXISTS tasks (
                id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                username VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
                email VARCHAR(255)  COLLATE utf8_unicode_ci  NOT NULL,
                filename VARCHAR(255) COLLATE utf8_unicode_ci ,
                content TEXT COLLATE utf8_unicode_ci,
                is_complete BOOLEAN NOT NULL DEFAULT 0,
                created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        $this->runQuery($sql, 'CREATE TABLE IF NOT EXISTS tasks');

    }

    /**
     * @param string $sql
     * @param string $caption
     */
    protected function runQuery(string $sql, string $caption)
    {
        if (static::$dbh->query($sql)) {
            echo "Команда `{$caption}` выполнена.\n";
        } else {
            echo "Ошибка при выполнении команды `{$caption}`:" . static::$dbh->error. "\n";
        }
    }
}

if ($argv['1']) {
    (new Migrate)->{$argv['1']}();
} else {
    echo "Не команды для выполнения.\n Доступна: up\n";
}