<?php
declare(strict_types=1);

namespace Helpers;

/**
 * Class SortHelper
 * @author Valentin Plehanov (Takamura) valentin@plehanov.pro
 * @package Helpers
 */
class SortHelper
{

    /**
     * @param array $get
     * @param array $default
     * @param array $filter
     *
     * @return array
     */
    public static function detectSort(array $get, array $default, array $filter): array
    {
        if (!array_key_exists('sort', $get)) {
            return $default;
        }

        $sort = explode('-', strtolower($get['sort']));
        if (!in_array($sort[1], ['up', 'down'], false)) {
            $sort[1] = 'up';
        }

        return in_array($sort[0], $filter, false) ? $sort : $default;
    }

    /**
     * @param array $sort
     *
     * @return string
     */
    public static function convertSortByOrder(array $sort): string
    {
        return "`{$sort[0]}` " . ($sort[1] === 'up' ? 'asc' : 'desc');
    }
}