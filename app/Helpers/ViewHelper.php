<?php
declare(strict_types=1);

namespace Helpers;

/**
 * Class ViewHelper
 * @author Valentin Plehanov (Takamura) valentin@plehanov.pro
 * @package Helpers
 */
class ViewHelper
{

    /**
     * @param string $sortName
     * @param string $sortType
     * @param string $currentSortName
     * @param string $urlPrefix
     * @param string $title
     *
     * @return string
     */
    public static function arrows(string $sortName, string $sortType, string $currentSortName, string $urlPrefix, string $title): string
    {
        if ($sortName === $currentSortName) {
            if ($sortType === 'up') {
                $str = "<a href='{$urlPrefix}&sort={$currentSortName}-down'>{$title} ⬆</a>";
            } elseif ($sortType === 'down') {
                $str = "<a href='{$urlPrefix}&sort={$currentSortName}-up'>{$title} ⬇</a>";
            }
        } else {
            $str = "<a href='{$urlPrefix}&sort={$currentSortName}-up'>{$title} &bull;</a>";
        }

        return $str;
    }

    /**
     * @param string $urlPrefix
     * @param int    $page
     * @param string $string
     *
     * @return string
     */
    public static function page(string $urlPrefix, int $page, string $string): string
    {
        return "<a href='{$urlPrefix}&page={$page}'>{$string}</a>";
    }
}
