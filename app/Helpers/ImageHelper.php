<?php
declare(strict_types=1);

namespace Helpers;

/**
 * Class ImageHelper
 * @author Valentin Plehanov (Takamura) valentin@plehanov.pro
 * @package Helpers
 */
class ImageHelper
{

    protected static $width = 320;
    protected static $height = 240;
    protected static $ratio = 320 / 240;

    public static function generateFileName(string $file, string $mimeType): string
    {
        return 'pic_' . md5($file) . '.' . static::getFileType($mimeType);
    }

    /**
     * @param string $mimeType
     * @return string
     */
    public static function getFileType(string $mimeType): string
    {
        return str_replace('image/', '', $mimeType);
    }

    /**
     * @param string $destinationFile
     * @param string $newFile
     * @param string $type
     * @return bool
     */
    public static function cropAndSave(string $destinationFile, string $newFile, string $type): bool
    {
        // FIXME 2017-07-08 нет защиты от иных типов кроме тех что прошли через ValidateHelper::isAvailableType,
        // если не использовать хелпер для валидации скрип упадет

        [$width, $height] = getimagesize($destinationFile);
        [$newWidth, $newHeight] = self::proportionalResize((int)$width, (int)$height);

        $status = false;

        if (move_uploaded_file($destinationFile, $newFile)) {

            switch($type) {
                case 'jpeg':
                    $image = imagecreatefromjpeg($newFile);
                    break;
                case 'gif':
                    $image = imagecreatefromgif($newFile);
                    break;
                case 'png':
                    $image = imagecreatefrompng($newFile);
            }

            $imagePlace = imagecreatetruecolor($newWidth, $newHeight);
            imagecopyresampled($imagePlace, $image, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

            switch ($type) {
                case 'jpeg':
                    $status = imagejpeg($imagePlace, $newFile, 95);
                    break;
                case 'png':
                    $status = imagepng($imagePlace, $newFile, 0);
                    break;
                case 'gif':
                    $status = imagegif($imagePlace, $newFile);
                    break;
            }
        }

        unlink($destinationFile);

        return $status;
    }

    /**
     * @param int $width
     * @param int $height
     * @return array
     */
    public static function proportionalResize(int $width, int $height): array
    {
        $currentRatio = $width / $height;
        $result = [static::$width, static::$height];

        if (static::$ratio < $currentRatio) {
            $result = [static::$width, (int)round(static::$width / $currentRatio)];
        } elseif (static::$ratio > $currentRatio) {
            $result = [(int)round(static::$height / $currentRatio), static::$height];
        }

        return $result;
    }
}
