<?php
declare(strict_types=1);

namespace Helpers;

/**
 * Class QueryHelper
 * @author Valentin Plehanov (Takamura) valentin@plehanov.pro
 * @package Helpers
 */
class QueryHelper
{
    protected static $prefix = 'p_';

    /**
     * @param array $attributes
     *
     * @return string
     */
    public static function insertKeys(array $attributes): string
    {
        $keys = array_keys(static::attributes($attributes));

        return implode(',', $keys);
    }

    /**
     * @param array $attributes
     *
     * @return string
     */
    public static function insertValues(array $attributes): string
    {
        $keys = array_keys(static::attributes($attributes));
        $keys = array_map(function ($attribute) {
            return static::setPrefix($attribute);
        }, $keys);

        return implode(',', $keys);
    }

    /**
     * @param array  $attributes
     * @param string $splitter
     * @param bool   $withoutID
     *
     * @return string
     */
    public static function pairs(array $attributes, string $splitter = ',', $withoutID = true): string
    {
        $attr = [];
        foreach (array_keys(static::attributes($attributes, $withoutID)) as $attribute) {
            $attr[] = "{$attribute}=" . self::setPrefix($attribute);
        }

        return implode($splitter, $attr);
    }

    /**
     * @param array $attributes
     * @param bool  $withoutID
     *
     * @return array
     */
    public static function prepareParams(array $attributes, bool $withoutID = true): array
    {
        $parameters = [];
        foreach (static::attributes($attributes, $withoutID) as $attribute => $value) {
            $parameters[static::setPrefix($attribute)] = $value;
        }

        return $parameters;
    }

    /**
     * @param string $attribute
     *
     * @return string
     */
    public static function setPrefix(string $attribute)
    {
        return ':' . static::$prefix . $attribute;
    }

    /**
     * @param array $attributes
     * @param bool  $withoutID
     *
     * @return array
     */
    protected static function attributes(array $attributes, bool $withoutID = true): array
    {
        if ($withoutID) {
            unset($attributes['id']);
        }

        return $attributes;
    }
}