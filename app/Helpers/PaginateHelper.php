<?php
declare(strict_types=1);

/**
 * User: Valentin Plehanov (Takamura) valentin@plehanov.pro
 */

namespace Helpers;


class PaginateHelper
{
    /**
     * @param array $get
     *
     * @return int
     */
    public static function detectPage(array $get): int
    {
        return array_key_exists('page', $get) ? (int)$get['page'] : 0;
    }

    /**
     * @param int $page
     * @param int $limit
     *
     * @return int
     */
    public static function getOffset(int $page, int $limit): int
    {
        if ($page < 0) {
            return 0;
        }

        return (int)round($page * $limit);
    }

    /**
     * @param int $total
     * @param int $limit
     *
     * @return int
     */
    public static function maxPage(int $total, int $limit): int
    {
        if ($total === 0) {
            return 0;
        }

        return (int)ceil($total / $limit);
    }
}