<?php
declare(strict_types=1);

namespace Helpers;

/**
 * Class ValidateHelper
 * @author Valentin Plehanov (Takamura) valentin@plehanov.pro
 * @package Helpers
 */
class ValidateHelper
{

    /**
     * @param string $value
     * @param string|null $default
     * @return string
     */
    public static function input(string $value, string $default = null): string
    {
        $value = trim((string)$value);
        $value = stripslashes($value);
        $value = htmlspecialchars($value);

        if ($value === '') {
            return $default;
        }

        return $value;
    }

    /**
     * @param string $value
     * @return bool
     */
    public static function isEmail(string $value): bool
    {
        return (bool)filter_var($value, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @param string $fileType
     * @return bool
     */
    public static function isAvailableType(string $fileType): bool
    {
        return in_array($fileType, ['image/jpeg', 'image/png', 'image/gif'], true);
    }
}