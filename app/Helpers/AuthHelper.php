<?php
declare(strict_types=1);

/**
 * User: Valentin Plehanov (Takamura) valentin@plehanov.pro
 */

namespace Helpers;


class AuthHelper
{
    /**
     * @return bool
     */
    public static function isAdmin(): bool
    {
        session_start(['cookie_lifetime' => 86400]);

        return isset($_SESSION['admin']) && $_SESSION['admin'];
    }

    /**
     * @param string $username
     * @param string $password
     *
     * @return bool
     */
    public static function checkAndSetAdmin(string $username, string $password): bool
    {
        $auth = self::loadConfig();

        if ($auth->username === strtolower($username) && $auth->password === strtolower($password)) {

            session_start(['cookie_lifetime' => 86400]);
            $_SESSION['admin'] = true;

            return true;
        }

        return false;
    }

    /**
     *
     */
    public static function unsetAdmin(): void
    {
        session_start();
        session_unset();
        session_destroy();
    }

    /**
     * @return \stdClass
     */
    protected static function loadConfig(): \stdClass
    {
        $auth = require APP_CONFIG . 'auth.php';

        return (object)$auth;
    }
}
