<?php
declare(strict_types=1);

namespace Models;

use Models\Repository\Repository;


/**
 * Class Task
 * User: Valentin Plehanov (Takamura) valentin@plehanov.pro
 * @package Models
 */
class Task extends Repository
{
    /**
     * @var string
     */
    protected $table = 'tasks';

}