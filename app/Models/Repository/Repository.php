<?php
declare(strict_types=1);

namespace Models\Repository;

use Helpers\QueryHelper;
use Traits\Connectible;


/**
 * Class Repository
 * @author Valentin Plehanov (Takamura) valentin@plehanov.pro
 * @package Models\Repository
 */
abstract class Repository
{
    use Connectible;

    protected $table = '';
    protected $fieldsForSelected = '*';
    protected $limit;
    protected $offset;
    protected $order;


    /**
     * @param int $value
     *
     * @return $this
     */
    public function setLimit(int $value)
    {
        $this->limit = $value;

        return $this;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function setOffset(int $value)
    {
        $this->offset = $value;

        return $this;
    }

    /**
     * @param array ...$parameters
     *
     * @return $this
     */
    public function orderBy(... $parameters)
    {
        $this->order = 'ORDER BY ' . implode(' ,', $parameters);

        return $this;
    }

    /**
     * @param array ...$fields
     *
     * @return $this
     */
    public function select(...$fields)
    {
        $f = array_map(function ($value) {
            return "`{$value}`";
        }, $fields);
        $this->fieldsForSelected = implode(',', $f);

        return $this;
    }

    /**
     * @param string $raw
     *
     * @return $this
     */
    public function selectRaw(string $raw)
    {
        $this->fieldsForSelected = $raw;

        return $this;
    }

    /**
     * @return array
     */
    public function all(): array
    {
        $sql = "SELECT {$this->fieldsForSelected} FROM `{$this->table}` {$this->order} {$this->buildLimitString()}";
        $query = static::$dbh->query($sql);

        return $query->fetchAll(\PDO::FETCH_CLASS);
    }

    /**
     * @return null|\stdClass
     */
    public function first(): ?\stdClass
    {
        $sql = "SELECT {$this->fieldsForSelected} FROM `{$this->table}` {$this->order} LIMIT 1";
        $query = static::$dbh->query($sql);

        return $query->fetchObject();
    }

    /**
     * @param int $id
     *
     * @return \stdClass|null
     */
    public function find(int $id): ?\stdClass
    {
        $sql = "SELECT {$this->fieldsForSelected} FROM `{$this->table}` WHERE id = ? {$this->order} {$this->buildLimitString()}";
        $query = static::$dbh->prepare($sql);
        $query->execute([$id]);
        $result = $query->fetchObject();

        return $result ?: null;
    }

    /**
     * @param array $parameters
     *
     * @return int|null
     */
    public function create(array $parameters): ?int
    {
        $sql = "INSERT INTO `{$this->table}` (" . QueryHelper::insertKeys($parameters) . ') VALUES(' . QueryHelper::insertValues($parameters) . ')';
        $query = static::$dbh->prepare($sql);

        return $query->execute(QueryHelper::prepareParams($parameters)) ? (int)static::$dbh->lastInsertId() : null;
    }

    /**
     * @param array $parameters
     * @param int $id
     *
     * @return bool
     */
    public function update(array $parameters, int $id): bool
    {
        $sql = "UPDATE `{$this->table}` SET " . QueryHelper::pairs($parameters) . ' WHERE id=' . QueryHelper::setPrefix('id');
        $query = static::$dbh->prepare($sql);
        $params = QueryHelper::prepareParams($parameters);
        $params[QueryHelper::setPrefix('id')] = $id;

        return $query->execute($params);
    }

    /**
     * @param array $parameters
     *
     * @return bool
     */
    public function delete(array $parameters): bool
    {
        $sql = "DELETE FROM `{$this->table}` WHERE " . QueryHelper::pairs($parameters, ' AND ', false) . " {$this->order} {$this->buildLimitString()}";
        $query = static::$dbh->prepare($sql);
        $params = QueryHelper::prepareParams($parameters, false);

        return $query->execute($params);
    }

    /**
     * @return string
     */
    protected function buildLimitString(): string
    {
        if (!$this->offset && !$this->limit) {
            return '';
        }
        return 'LIMIT ' . (int)$this->offset . ',' . (int)$this->limit;
    }
}
