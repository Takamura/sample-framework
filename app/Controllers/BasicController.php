<?php
declare(strict_types=1);

namespace Controllers;

use Helpers\ImageHelper;
use Helpers\PaginateHelper;
use Helpers\SortHelper;
use Helpers\ValidateHelper;
use Models\Task;


/**
 * Class BasicController
 * @author Valentin Plehanov (Takamura) valentin@plehanov.pro
 * @package Controllers
 */
class BasicController
{
    /**
     * @return string
     */
    public function index(): string
    {
        $page = PaginateHelper::detectPage($_GET);
        [$sortName, $sortType] = SortHelper::detectSort($_GET, ['username', 'up'], ['username', 'email', 'is_complete']);
        $limit = 3;
        $offset = PaginateHelper::getOffset((int)$page, $limit);

        $tasks = (new Task)
            ->select('id', 'username', 'email', 'content', 'filename', 'is_complete', 'created_at')
            ->setOffset($offset)
            ->setLimit($limit)
            ->orderBy(SortHelper::convertSortByOrder([$sortName, $sortType]))
            ->all();

        $totalObj = (new Task)
            ->selectRaw('count(id) as total')
            ->first();
        $maxPage = PaginateHelper::maxPage($totalObj->total, $limit) - 1;

        return view('home', compact('tasks', 'page', 'maxPage', 'sortName', 'sortType'));
    }

    /**
     * @return string
     */
    public function create(): string
    {
        $params = $this->prepareTaskParams();
        $params['filename'] = $this->prepareTaskImage();

        $id = $params ? (new Task)->create($params) : null;

        return view($id ? 'task.created' : 'task.failed');
    }

    /**
     * @return string
     */
    public function edit(): string
    {
        $id = isset($_GET['id']) ? ValidateHelper::input($_GET['id']) : null;

        if (!$id) {
            \Route::redirect();
        }
        $task = (new Task)->find((int)$id);
        if (!$task) {
            \Route::redirect();
        }

        return view('task.edit', compact('task'));
    }

    /**
     * @return string
     */
    public function update(): string
    {
        $id = isset($_GET['id']) ? (int)ValidateHelper::input($_GET['id']) : null;

        if (!$id) {
            \Route::redirect();
        }

        $params = $this->prepareTaskParams();
        $params['is_complete'] = (int)(isset($_POST['task']['is_complete']) && $_POST['task']['is_complete'] === 'on');
        $filename = $this->prepareTaskImage();

        if ($params || $filename) {
            $task = (new Task)->find((int)$id);
            if ($task) {
                if ($filename) {
                    unlink(APP_UPLOADS . $task->filename);
                    $params['filename'] = $filename;
                }
                (new Task)->update($params, $id);
            }
        }

        \Route::redirect();
    }


    /**
     * @return string
     */
    public function delete(): string
    {
        $id = isset($_GET['id']) ? (int)ValidateHelper::input($_GET['id']) : null;

        if (!$id) {
            \Route::redirect();
        }

        (new Task)->delete(['id' => $id]);

        \Route::redirect();
    }

    /**
     * @return array|null
     */
    protected function prepareTaskParams(): ?array
    {
        if (!$_POST['task']) {
            return null;
        }
        $taskRaw = $_POST['task'];

        $task = [
            'content' => ValidateHelper::input($taskRaw['content'], ''),
            'username' => ValidateHelper::input($taskRaw['username'], '')
        ];

        $email = ValidateHelper::input($taskRaw['email'], '');
        if (ValidateHelper::isEmail($email)) {
            $task['email'] = $email;
        }

        return $task;
    }

    /**
     * @return null|string
     */
    protected function prepareTaskImage(): ?string
    {
        if ($_FILES['task'] &&
            $_FILES['task']['size']['file'] > 0 &&
            $_FILES['task']['error']['file'] === 0 &&
            ValidateHelper::isAvailableType($_FILES['task']['type']['file'])
        ) {
            $mimeType = $_FILES['task']['type']['file'];
            $name = ImageHelper::generateFileName($_FILES['task']['name']['file'], $mimeType);

            return ImageHelper::cropAndSave($_FILES['task']['tmp_name']['file'], APP_UPLOADS . $name,
                ImageHelper::getFileType($mimeType)) ? $name : null;
        }

        return null;
    }
}
