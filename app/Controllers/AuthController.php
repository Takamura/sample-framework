<?php
declare(strict_types=1);

namespace Controllers;

use Helpers\AuthHelper;
use Helpers\ValidateHelper;


/**
 * Class AuthController
 * @author Valentin Plehanov (Takamura) valentin@plehanov.pro
 * @package Controllers
 */
class AuthController
{

    /**
     *
     */
    public function login(): void
    {
        if (!$_POST['username'] && !$_POST['password']) {
            \Route::redirect();
        }
        $username = ValidateHelper::input($_POST['username'], '');
        $password = ValidateHelper::input($_POST['password'], '');

        AuthHelper::checkAndSetAdmin($username, $password);

        \Route::redirect();
    }

    /**
     *
     */
    public function logout(): void
    {
        AuthHelper::unsetAdmin();

        \Route::redirect();
    }
}
